import axios from 'axios';

export default class ProductoService {
    url = "http://localhost:9000/api/producto/";
    config = {
        headers: {
            'Content-type': 'application/json',
            'Access-Control-Allow-Origin':'*',
            'Access-Control-Allow-Methods': '*',
            'Access-Control-Allow-Headers':'Content-Type',
           }
     };

    obtenerTodosProductos(){
        return axios.get("http://localhost:9000/api/producto/listarproducto");
    }
}
