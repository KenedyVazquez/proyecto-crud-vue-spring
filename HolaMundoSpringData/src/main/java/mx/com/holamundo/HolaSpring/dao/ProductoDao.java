/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.holamundo.HolaSpring.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import mx.com.holamundo.HolaSpring.domain.productoModelo;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 *
 * @author Kenedy
 */
@Repository
public interface ProductoDao extends JpaRepository<productoModelo, Integer> {

    @Query("select p from productoModelo p where p.idproducto = ?1")
    Optional<productoModelo> buscarProductoPorId(Integer idProducto);
    
}
