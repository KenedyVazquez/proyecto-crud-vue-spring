package mx.com.holamundo.HolaSpring.dto;

import mx.com.holamundo.HolaSpring.domain.productoModelo;

import java.io.Serializable;

public class ProductoDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private Integer cantidad;
    private String descripcion;
    private String producto;

    public ProductoDTO() {
        super();
    }

    public ProductoDTO(Integer id, String producto) {
        super();
        this.id = id;
        this.producto = producto;
    }

    public ProductoDTO(productoModelo item) {
        this.id = item.getIdproducto();
        this.cantidad = item.getCantidad();
        this.descripcion = item.getDescripcion();
        this.producto = item.getProducto();
    }


    public Integer getId() {
        return id;
    }

    public ProductoDTO setId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public ProductoDTO setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
        return this;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public ProductoDTO setDescripcion(String descripcion) {
        this.descripcion = descripcion;
        return this;
    }

    public String getProducto() {
        return producto;
    }

    public ProductoDTO setProducto(String producto) {
        this.producto = producto;
        return this;
    }
}
