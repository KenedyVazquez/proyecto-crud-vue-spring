/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.holamundo.HolaSpring.domain;

import java.io.Serializable;
import javax.persistence.*;
import lombok.Data;

/**
 *
 * @author Kenedy
 */
@Entity
@Table(name = "producto")
public class productoModelo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idproducto;
    private Integer cantidad;
    private String descripcion;
    private String producto;

    public Integer getIdproducto() {
        return idproducto;
    }

    public productoModelo setIdproducto(Integer idproducto) {
        this.idproducto = idproducto;
        return this;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public productoModelo setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
        return this;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public productoModelo setDescripcion(String descripcion) {
        this.descripcion = descripcion;
        return this;
    }

    public String getProducto() {
        return producto;
    }

    public productoModelo setProducto(String producto) {
        this.producto = producto;
        return this;
    }
}
