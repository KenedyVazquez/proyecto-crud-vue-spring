/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.holamundo.HolaSpring.web;

import java.util.List;
import java.util.Optional;

import mx.com.holamundo.HolaSpring.dao.ProductoDao;
import mx.com.holamundo.HolaSpring.dto.ProductoDTO;
import mx.com.holamundo.HolaSpring.servicio.ProductoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import mx.com.holamundo.HolaSpring.domain.productoModelo;

/**
 * @author Kenedy
 */
@RestController
@RequestMapping("/api/producto/")
public class ProductoREST {
    @Autowired
    private ProductoDao productoDao;

    @Autowired
    private ProductoServicio productoServicio;

    @GetMapping("listarproducto")
    private List<ProductoDTO> listaProducto() {
        return productoServicio.obtenerProductos();
    }

    @GetMapping(value = "obtenerId")
    private ProductoDTO obtenerIdProducto(@RequestParam Integer idproducto) {
        return productoServicio.obtenerProductoPotId(idproducto);
    }

    @GetMapping(value = "eliminarId/{idproducto}")
    private void eliminarIdProducto(@PathVariable("idproducto") Integer idproducto) {
        productoDao.deleteById(idproducto);
    }

    @PostMapping("guardarproducto")
    private productoModelo guardarProducto(@RequestBody productoModelo producto) {
        productoModelo temporal = productoDao.save(producto);
        return temporal;
    }
}
