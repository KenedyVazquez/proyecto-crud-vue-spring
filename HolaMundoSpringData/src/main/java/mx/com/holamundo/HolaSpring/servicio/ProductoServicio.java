package mx.com.holamundo.HolaSpring.servicio;

import mx.com.holamundo.HolaSpring.dao.ProductoDao;
import mx.com.holamundo.HolaSpring.domain.productoModelo;
import mx.com.holamundo.HolaSpring.dto.ProductoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductoServicio implements Serializable {
    private static final long serialVersionUID = 1L;

    @Autowired
    private ProductoDao productoDao;

    public List<ProductoDTO> obtenerProductos(){
        List<productoModelo> productos = productoDao.findAll();

        return productos.stream().map(
                ProductoDTO::new
        ).collect(Collectors.toList());

    }


    public ProductoDTO obtenerProductoPotId(Integer idproducto) {
        Optional<productoModelo> producto = productoDao.buscarProductoPorId(idproducto);
        if (!producto.isPresent()){
            return null;
        }
        return new ProductoDTO(producto.get());
    }
}
