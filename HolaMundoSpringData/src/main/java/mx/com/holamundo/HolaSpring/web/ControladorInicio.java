/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.holamundo.HolaSpring.web;

import lombok.extern.slf4j.Slf4j;
import mx.com.holamundo.HolaSpring.dao.ProductoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author Kenedy
 */
//Para indicarle a spring que es la clase es de tipo spring controlle
@Controller
//libreria de lommbok para log();
@Slf4j
public class ControladorInicio {
    @Autowired
    private ProductoDao productoDao;
    @GetMapping("/")
    public String Inicio(Model model){
        var personas =  productoDao.findAll();
        model.addAttribute("personas", personas);
        return "index" ;
    }
}
